## '#' begins with a comment, and the options have corresponding comments, modified as needed ##
## The commented option uses the default value, it is recommended to uncomment when needed. ##

## RPC related settings ##

# Enable RPC, default: false
enable-rpc=true
# background operation
daemon=false
# Allow all sources, default: false
rpc-allow-origin-all=true
# Allow non-external access, default: false
rpc-listen-all=true
# Event polling mode, value: [epoll, kqueue, port, poll, select], different system default values ​​are different
#event-poll=select
# RPC listening port, can be modified when the port is occupied, default: 6800
rpc-listen-port=6800
# Set the RPC authorization token, v1.18.4 new, replace the --rpc-user and --rpc-passwd options
rpc-secret=000000
# Set the RPC access user name (1.15.2 or higher, 1.18.6 or lower version), the new version of this option is obsolete, it is recommended to use the --rpc-secret option instead.
#rpc-user=<USER>
# Set the RPC access password (1.15.2 or higher, 1.18.6 or lower). The new version of this option is deprecated. It is recommended to use the --rpc-secret option instead.
#rpc-passwd=<PASSWD>
# Whether to enable SSL/TLS encryption for RPC services,
# After enabling encryption, the RPC service needs to be connected using https or wss protocol.
#rpc-secure=true
# Certificate file (.pem/.crt) when SSL/TLS encryption is enabled in the RPC service
#rpc-certificate=./xxx.pem
# Private key file (.key) when SSL/TLS encryption is enabled in the RPC service
#rpc-private-key=./xxx.key

## File Save Related ##

# File save path (can use absolute path or relative path), default: current start position
dir=./data
# Enable disk cache, 0 is disable cache, need version 1.16 or later, default: 16M
#disk-cache=32M
# File pre-allocation mode, can effectively reduce disk fragmentation, default: prealloc
#Preallocation time required: none < falloc ? trunc < prealloc
# falloc and trunc require file system and kernel support
# NTFS, EXT4 It is recommended to use falloc, EXT3 recommended trunc, MAC need to comment this item
file-allocation=falloc
# http
continue=true
# Get server file time, default: false
remote-time=true

## Download Connection Related ##

# Maximum number of simultaneous download tasks, can be modified at runtime, default: 5, route suggested value: 3
max-concurrent-downloads=5
# Same server connection number, can be specified when adding, default: 1
max-connection-per-server=5
# Minimum file fragment size, can be specified when adding, the range is 1M -1024M, default: 20M
# Assume size=10M, file 20MiB is downloaded using two sources; file is 15MiB using one source download
#min-split-size=20M
# Maximum number of threads per task, can be specified when adding, Default: 5, route suggested value: 5
split=16
# Overall download speed limit, can be modified at runtime, default: 0
#max-overall-download-limit=0
# Single task download speed limit, default: 0
#max-download-limit=0
# Overall upload speed limit, can be modified during runtime, default: 0
max-overall-upload-limit=1M
# Single task upload speed limit, default: 0
#max-upload-limit=1000
# Disable IPv6, default: false
disable-ipv6=false
#Support GZip, default: false
http-accept-gzip=true

## Progress save related ##

# Read the download task from the session file
input-file=./.aria2/aria2.session
# Save the 'error/unfinished' download task to the session file when exiting Aria2
save-session=./.aria2/aria2.session
# Timely save session, 0 is saved when exiting, need to be 1.16.1 or later, default: 0
save-session-interval=1
# Force the session to be saved, even if the task has been completed, default: false
# The newer version will still retain the .aria2 file after the task is completed.
force-save=true

## BT/PTDownload Related ##

# When the download is a seed (ending with .torrent), the BT task is automatically started. Default: true, optional: false|mem
#follow-torrent=true
# BT listening port, used when the port is blocked, default: 6881-6999
listen-port=51413
# Single seed maximum number of connections, 0 is unlimited, default: 55
bt-max-peers=0
# DHT(IPv4) file
dht-file-path=./.aria2/dht.dat
# DHT(IPv6) file
dht-file-path6=./.aria2/dht6.dat
# Open DHT function, PT needs to be disabled, default: true
enable-dht=true
# Open IPv6 DHT function, PT needs to be disabled
enable-dht6=true
# DHT network listening port, default: 6881-6999
#dht-listen-port=6881-6999
# Local node lookup, PT needs to be disabled, default: false
bt-enable-lpd=true
#种子交换, PT needs to be disabled, default: true
enable-peer-exchange=true
# Expect download speed, Aria2 will temporarily increase the number of connections to increase the download speed, in units of K or M. Default: 50K
bt-request-peer-speed-limit=10M
# Client masquerading, PT needs to keep the two parameters of user-agent and peer-agent consistent.
user-agent=Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3602.2 Safari/537.36
#user-agent=qBittorrent v4.1.3
peer-agent=qBittorrent v4.1.3
peer-id-prefix=-qB4130-
#peer-agent=uTorrentMac/1870(43796)
#peer-id-prefix=-UM1870-
#peer-agent=Deluge 1.3.15
#peer-id-prefix=-DE13F0-
#peer-agent=Transmission/2.92
#peer-id-prefix=-TR2920-
# When the seed sharing rate reaches this number, it automatically stops seeding, 0 is always seeding, default: 1.0
seed-ratio=1.0
# Minimum seeding time. When this option is set to 0, it will not be seeded after the BT task is downloaded.
seed-time=0
# BT check related, default: true
#bt-hash-check-seed=true
# When you continue the previous BT task, you do not need to verify again. Default: false
#bt-seed-unverified=true
# Save the magnetic link metadata as a seed file (.torrent file), default: false
bt-save-metadata=true
# Load saved metadata file, default: false
bt-load-saved-metadata=true
# Delete unselected files, default: false
bt-remove-unselected-file=true
# Save the uploaded seed, default: true
#rpc-save-upload-metadata=false

## Execute additional commands ##

# Download the command executed after the stop (download stop contains the two states of download error and download completion, if there is no separate setting, execute this command.)
# Delete files and .aria2 suffix files
on-download-stop=./.aria2/delete.sh
# The command executed after downloading the error (the download stop contains the status of the download error, and if it is not set or commented, the command executed after the download is stopped is executed.)
#on-download-error=
# Download the command executed after the completion (download stop includes the download completion status, if not set or commented, execute the command executed after the download is stopped.)
# Delete.aria2 suffix file
#on-download-complete=./.aria2/delete.aria2.sh
# Call rclone upload (move) to the network disk
on-download-complete=./.aria2/autoupload.sh
# Download the command executed after the pause
# Display download task information
#on-download-pause=./.aria2/info.sh
# Download the command executed after the start
#on-download-start=

## BTServer ##
bt-tracker=
